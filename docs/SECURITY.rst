Security Policy
===============

Supported Versions
------------------

+---------+---------------------+
| Version | Status              |
+=========+=====================+
| 0.22.x  | Stable version      |
+---------+---------------------+
| 0.21.x  | Security fixes only |
+---------+---------------------+
| < 0.21  | not supported       |
+---------+---------------------+

Reporting a Vulnerability
-------------------------

Please disclose security vulnerabilities privately at miurahr@linux.com
